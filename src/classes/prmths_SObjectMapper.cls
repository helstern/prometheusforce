public with sharing class prmths_SObjectMapper 
{
    public interface SocialProfileMatchMapping 
    {
        Schema.SObjectField getTargetField();

        Object getValue(prmths_ApiModels.SocialProfileMatch sourceProfileMatch);
    }

    public class DisplayNameMapping implements SocialProfileMatchMapping
    {
        private Schema.SObjectField targetField;

        public DisplayNameMapping(Schema.SObjectField targetField)            
        {
            this.targetField = targetField;
        }

        public Schema.SObjectField getTargetField()
        {
            return targetField;
        }

        public Object getValue(prmths_ApiModels.SocialProfileMatch sourceProfileMatch)
        {
            return sourceProfileMatch.profile.display_name;
        }
    }

    public class SimpleCopyMapper
    {
        private List<Schema.SObjectField> copyFields;

        private List<SocialProfileMatchMapping> socialProfileMappings;

        public SimpleCopyMapper(
            List<Schema.SObjectField> targetFields,
            List<SocialProfileMatchMapping> socialProfileMappings
        ){
            this.copyFields = targetFields;            
            this.socialProfileMappings = socialProfileMappings;            
        }

        private SObject loadMappedField(SObject targetObject)
        {
            String separator = '';
            String fieldNames = '';
            for (Schema.SObjectField targetField: copyFields) {
                fieldNames += separator + targetField.getDescribe().getName();
                separator = ', ';
            }            

            String mappedFieldsQueryFormat = 'SELECT {0} FROM {1} WHERE Id = \'\'{2}\'\'';
            String mappedFieldsQuery = String.format(
                mappedFieldsQueryFormat, 
                new String[]{
                    fieldNames,
                    targetObject.getSObjectType().getDescribe().getName(),
                    targetObject.Id
                }
            );

            System.debug(mappedFieldsQuery);
            System.debug(targetObject);


            SObject refreshedObject;        
            try {
                List<SObject> results = System.Database.query(mappedFieldsQuery);
                refreshedObject = results.get(0);
            } catch (QueryException e){
                throw e;    
            }    

            return refreshedObject;
        }

        public SObject mapObject(
            SObject targetObject, 
            SObject sourceObject, 
            prmths_ApiModels.SocialProfileMatch sourceProfileMatch
        ) {
            if (! copyFields.isEmpty()) {
                mapFromSourceObject(targetObject, sourceObject);
            }

            if (! socialProfileMappings.isEmpty()) {
                mapFromSourceProfileMatch(targetObject, sourceProfileMatch);
            }

            return targetObject;            
        }        

        private void mapFromSourceObject(            
            SObject targetObject, 
            SObject sourceObject
        ) {

            System.debug(
                JSON.serialize(targetObject)
            );

            SObject fullyLoadedSource = loadMappedField(sourceObject);

            Object fieldValue;
            for (Schema.SObjectField targetField: copyFields) {
                fieldValue = fullyLoadedSource.get(targetField);
                targetObject.put(targetField, fieldValue);
            }
        }

        private void mapFromSourceProfileMatch(
            SObject targetObject, 
            prmths_ApiModels.SocialProfileMatch sourceProfileMatch
        ) {
            Object fieldValue;
            Schema.SObjectField targetField;
            for (SocialProfileMatchMapping mapping: socialProfileMappings) {
                fieldValue = mapping.getValue(sourceProfileMatch);

                targetField = mapping.getTargetField();
                targetObject.put(targetField, fieldValue);
            }
        }
    }

    public static SimpleCopyMapper createLeadCopyMapper()
    {
        Schema.SObjectType objectType = Lead.sObjectType; 

        Set<Schema.SObjectField> blackListedFields = new Set<Schema.SObjectField>();
        blackListedFields.add(Lead.Id);

        List<Schema.SObjectField> mappedFields = new List<Schema.SObjectField>();
        for (Schema.SObjectField field: objectTYpe.getDescribe().fields.getMap().values()) {
            if (
                ! field.getDescribe().isExternalId()
                && field.getDescribe().isUpdateable()
                && field.getDescribe().isCreateable()
                && ! blackListedFields.contains(field)
            ) {
                mappedFields.add(field);
            }
        }

        List<SocialProfileMatchMapping> profileMappings;
        profileMappings = new List<SocialProfileMatchMapping>{
            new DisplayNameMapping(Lead.FirstName),
            new DisplayNameMapping(Lead.LastName)
        };
    
        return new SimpleCopyMapper(mappedFields, profileMappings);        
    }

	public prmths_SObjectMapper() 
    {
    }
}