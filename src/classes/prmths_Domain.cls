public with sharing class prmths_Domain {
    
    public interface SpecificationExecutor
    {
        List<sObject> toList();

        sObject one();
    }
}