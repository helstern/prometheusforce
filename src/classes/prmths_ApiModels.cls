public with sharing class prmths_ApiModels {

    public class Oauth2AccessGoogleCredential {

        public String access_token {get; set;}

        public Integer expires_in {get; set;}

        public String refresh_token {get; set;}
    }

    public class SocialProfileSummary 
    {
        public String display_name {get; set;}

        public String image {get; set;}
        
        public String id {get; set;}        
    }

    public class SocialProfileMatch 
    {
        public Integer score {get; set;}

        public SocialProfileSummary profile {get; set;}
    }
}