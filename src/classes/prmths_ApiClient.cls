public class prmths_ApiClient
{
	public virtual class ApiException extends Exception
	{
		
	}

	public class ApiResponseException extends ApiException
	{
		private HttpResponse response;

		public ApiResponseException(String message, HttpResponse response)
		{
			this(message);

			this.response = response;
		}

		public HttpResponse getResponse()
		{
			return response;
		}
	}

	public class InvalidArgumentException extends Exception 
	{}

    public class HeaderInjector 
    {    	
    	private String headerName;

    	private String headerValue;

    	public HeaderInjector(String headerName, String headerValue)
    	{
    		this.headerName = headerName;
    		this.headerValue = headerValue;
    	}

    	public String getInjectableHeaderName() 
    	{
    		return headerName;
    	}

    	public void injectHeader(HttpRequest request)
    	{
			request.setHeader(headerName, headerValue);
    	}    	
    }

	public interface HeaderInjectorChain
	{
		HeaderInjectorChain getPrevious();

		HeaderInjectorChain next(HeaderInjector next);

		HeaderInjectorChain next(String headerName, String headerValue);

    	void injectHeader(HttpRequest request);
	}    		

	public class HeaderInjectorChainStart implements HeaderInjectorChain
	{
		public HeaderInjectorChainStart() 
		{}						

		public HeaderInjectorChain getPrevious()
		{
			return null;
		}

		public HeaderInjectorChain next(HeaderInjector next) 
		{
			HeaderInjectorChain chain = new HeaderInjectorChainLink(next, this);
			return chain;
		}

		public HeaderInjectorChain next(String headerName, String headerValue)
		{
			HeaderInjector next = new HeaderInjector(headerName, headerValue);
			HeaderInjectorChain chain = new HeaderInjectorChainLink(next, this);
			return chain;			
		}

    	public void injectHeader(HttpRequest request)
    	{}	    	
	}

	public class HeaderInjectorChainLink implements HeaderInjectorChain
	{
		private HeaderInjector headerInjector;

		private HeaderInjectorChain previousEntry;

		public HeaderInjectorChainLink(HeaderInjector headerInjector) 
		{
			this.headerInjector = headerInjector;
		}				

		public HeaderInjectorChainLink(HeaderInjector headerInjector, HeaderInjectorChain previous) 
		{
			this.headerInjector = headerInjector;
			this.previousEntry = previous;
		}		

		public HeaderInjectorChain getPrevious()
		{
			return previousEntry;
		}

		public HeaderInjectorChain next(HeaderInjector next) 
		{
			HeaderInjectorChain chain = new HeaderInjectorChainLink(next, this);
			return chain;
		}

		public HeaderInjectorChain next(String headerName, String headerValue)
		{
			HeaderInjector next = new HeaderInjector(headerName, headerValue);
			HeaderInjectorChain chain = new HeaderInjectorChainLink(next, this);
			return chain;			
		}

    	public void injectHeader(HttpRequest request)
    	{
			headerInjector.injectHeader(request);
			previousEntry.injectHeader(request);
    	}	    	
	}
	
    public interface HttpContentInjector 
    {
    	List<String> getInjectableHeaderNames();

		void injectContent(HttpRequest request);    	
    }

	public class NoContent implements HttpContentInjector
	{
		public NoContent() 
		{}

		public List<String> getInjectableHeaderNames() 
		{
			return new List<String> {};
		}

		public void injectContent(HttpRequest request) 
		{}		
	}

	public class HttpZeroLengthContent implements HttpContentInjector
	{
		public HttpZeroLengthContent() 
		{}

		public List<String> getInjectableHeaderNames() 
		{
			return new List<String> {ffhttp_Client.HTTP_HEADER_CONTENT_LENGTH};
		}

		public void injectContent(HttpRequest request) {
			request.setHeader(ffhttp_Client.HTTP_HEADER_CONTENT_LENGTH, String.valueOf(0));
			request.setBody('');				
		}
	}

	public class HttpBlobContent implements HttpContentInjector
	{
		private Blob content;

		private String contentType;

		public HttpBlobContent(Blob content, String contentType) 
		{
			this.content = content;				
			this.contentType = contentType;
		}

		public List<String> getInjectableHeaderNames() 
		{
			return new List<String> {ffhttp_Client.HTTP_HEADER_CONTENT_TYPE, ffhttp_Client.HTTP_HEADER_CONTENT_LENGTH};
		}

		public void injectContent(HttpRequest request) {

			request.setHeader(ffhttp_Client.HTTP_HEADER_CONTENT_TYPE, contentType);
			Integer bodySize = content.size();
			request.setHeader(ffhttp_Client.HTTP_HEADER_CONTENT_LENGTH, String.valueOf(bodySize));

			String bodyStr = content.toString();
			request.setBody(bodyStr);				
		}
	}

	public class HttpStringContent implements HttpContentInjector
	{
		private String content;

		private String contentType;

		public HttpStringContent(String content, String contentType) 
		{
			this.content = content;				
			this.contentType = contentType;
		}

		public List<String> getInjectableHeaderNames() 
		{
			return new List<String> {ffhttp_Client.HTTP_HEADER_CONTENT_TYPE, ffhttp_Client.HTTP_HEADER_CONTENT_LENGTH};
		}

		public void injectContent(HttpRequest request) {

			request.setHeader(ffhttp_Client.HTTP_HEADER_CONTENT_TYPE, contentType);
			Integer contentLength = content.length();
			request.setHeader(ffhttp_Client.HTTP_HEADER_CONTENT_LENGTH, String.valueOf(contentLength));

			request.setBody(content);			
		}
	}

	public class ResourceRepresentation 
	{
		private PageReference url;

		private HttpContentInjector contentInjector;

		public ResourceRepresentation(PageReference url, HttpContentInjector contentInjector) 
		{
			this.url = url;
			this.contentInjector = contentInjector;
		}

		public void inject (HttpRequest request) 
		{	
			String endpoint = url.getUrl();
			request.setEndpoint(endpoint);

			contentInjector.injectContent(request);
		}
	}	

	public class RequestPayload 
	{
		String requestMethod;

		ResourceRepresentation representation;

		public RequestPayload(String requestMethod, ResourceRepresentation representation) 
		{
			this.requestMethod = requestMethod;
			this.representation = representation;			
		}

		public void inject(HttpRequest request) {
			representation.inject(request);			
			request.setMethod(requestMethod);
		}
	}

	public class ResourceInvocation 
	{
		private prmths_ApiClient client;

		private RequestPayload payload;

		private HeaderInjectorChain headers;

		public ResourceInvocation(prmths_ApiClient client, RequestPayload payload, HeaderInjectorChain headers) 
		{
			this.client = client;
			this.payload = payload;
			this.headers = headers;
		}

		private void configureRequest(HttpRequest request)
		{
			payload.inject(request);
			headers.injectHeader(request);
		}

		public HttpResponse execute() 
		{
			HttpRequest request = new HttpRequest();
			configureRequest(request);			

			Http httpClient = new Http();
			HttpResponse response = httpClient.send(request);	

			Integer statusCode = response.getStatusCode();
			if (statusCode < 300) {
				return response;	
			}

			prmths_ApiClient.ApiResponseException ex;
			ex = new prmths_ApiClient.ApiResponseException('prometheus api error', response);
			throw ex;
		}

		public HttpResponse executeThenParse() 
		{
			HttpRequest request = new HttpRequest();
			configureRequest(request);

			Http httpClient = new Http();
			HttpResponse response = httpClient.send(request);

			Integer statusCode = response.getStatusCode();
			if (statusCode < 300) {
				return response;	
			}

			prmths_ApiClient.ApiResponseException ex;
			ex = new prmths_ApiClient.ApiResponseException('prometheus api error', response);
			throw ex;			
		}
	}


	public class ContentNegotiationBuilder 
	{
		private prmths_ApiClient client;

		private RequestPayload payload;

		private HeaderInjectorChain headers;

		// Accept
		private String mediaTypes = '*/*';

		// Accept-Charset
		private String charset = 'UTF-8';

		// Accept-Encoding
		private String encoding = 'identity';

		// Accept-Language
		private String language = 'en';

		public ContentNegotiationBuilder(prmths_ApiClient client, RequestPayload payload, HeaderInjectorChain headers)
		{
			this.client = client;
			this.payload = payload;
			this.headers = headers;
		}

		public ResourceInvocation send() 
		{
			HeaderInjectorChain nextHeaders = headers
				.next('Accept', this.mediaTypes)
				.next('Accept-Charset', this.charset)
				.next('Accept-Encoding', this.encoding)
				.next('Accept-Language', this.language);

			ResourceInvocation invocation = new ResourceInvocation(client, payload, headers);
			return invocation;
		}
	}

	public class AuthenticationBuilder
	{
		private prmths_ApiClient client;

		private RequestPayload payload;	

		public AuthenticationBuilder(prmths_ApiClient client, RequestPayload payload)
		{
			this.client = client;
			this.payload = payload;			
		}

		public ContentNegotiationBuilder accept(String contentType)
		{			
			HeaderInjectorChain headers = new HeaderInjectorChainStart();
			ContentNegotiationBuilder nextBuilder = new ContentNegotiationBuilder(client, payload, headers);
			return nextBuilder;
		}
	}

	public class PayloadBuilder 
	{		
		private prmths_ApiClient client;

		private String method;

		private ResourceRepresentation representation;

		public PayloadBuilder(prmths_ApiClient client, String method, ResourceRepresentation representation) 
		{
			this.client = client;
			this.method = method;
			this.representation = representation;
		}

		public ContentNegotiationBuilder accept(String contentType)
		{
			RequestPayload requestPayload = new RequestPayload(method, representation);
			HeaderInjectorChain headers = new HeaderInjectorChainStart();

			ContentNegotiationBuilder nextBuilder = new ContentNegotiationBuilder(client, requestPayload, headers);
			return nextBuilder;
		}

		public AuthenticationBuilder authenticate()  
		{
			RequestPayload payload = new RequestPayload(method, representation);		
			AuthenticationBuilder nextBuilder = new AuthenticationBuilder(client, payload);
			return nextBuilder;
		}
	} 

	public class RequestBuilder 
	{
		private prmths_ApiClient client;

		private PageReference endpoint;

		public RequestBuilder(prmths_ApiClient client, PageReference endpoint) 
		{
			this.client = client;			
			this.endpoint = endpoint;
		}


		public RequestBuilder queryParam(String name, String value) 
		{
			String url = this.endpoint.getUrl();
			PageReference newEndpoint = new PageReference(url);

			RequestBuilder newBuilder = new RequestBuilder(client, newEndpoint);
			return newBuilder;
		}

		public PayloadBuilder del() 
		{									
			HttpZeroLengthContent content = new HttpZeroLengthContent();
			ResourceRepresentation representation = new ResourceRepresentation(endpoint, content);

			return new PayloadBuilder(client, 'DELETE', representation);

		}

		public PayloadBuilder get() {		
			NoContent content = new NoContent();
			ResourceRepresentation representation = new ResourceRepresentation(endpoint, content);

			return new PayloadBuilder(client, 'GET', representation);
		}

		//public PayloadBuilder post(Map <String, String> form) {

		//}

		public PayloadBuilder post(ffhttp_IHttpContent content) 
		{
			Object body = content.getHttpContent();
			String type = content.getHttpContentType();			

			if (body instanceof Blob) {
				return this.post((Blob) body, type);
			} 

			if (body instanceof String) {
				return this.post((Blob) body, type);
			}

			throw new InvalidArgumentException();			
		}

		public PayloadBuilder post(Blob body, String contentType) 
		{
			HttpBlobContent content = new HttpBlobContent(body, contentType);
			ResourceRepresentation representation = new ResourceRepresentation(endpoint, content);

			return new PayloadBuilder(client, 'POST', representation);
		}

		public PayloadBuilder post(String body, String contentType) 
		{
			HttpStringContent content = new HttpStringContent(body, contentType);
			ResourceRepresentation representation = new ResourceRepresentation(endpoint, content);

			return new PayloadBuilder(client, 'POST', representation);
		}

		//public PayloadBuilder put(Map <String, String> form) 
		//{

		//}
	
		public PayloadBuilder put(ffhttp_IHttpContent content) 
		{
			Object body = content.getHttpContent();
			String type = content.getHttpContentType();			

			if (body instanceof Blob) {
				return this.post((Blob) body, type);
			} 

			if (body instanceof String) {
				return this.post((Blob) body, type);
			}

			throw new InvalidArgumentException();			
		}

		public PayloadBuilder put(Blob body, String contentType) 
		{			
			HttpBlobContent content = new HttpBlobContent(body, contentType);
			ResourceRepresentation representation = new ResourceRepresentation(endpoint, content);

			return new PayloadBuilder(client, 'PUT', representation);
		}

		public PayloadBuilder put(String body, String contentType) 
		{
			HttpStringContent content = new HttpStringContent(body, contentType);
			ResourceRepresentation representation = new ResourceRepresentation(endpoint, content);

			return new PayloadBuilder(client, 'PUT', representation);
		}		
	}

	static private final String host = 'prometheus-1025.appspot.com';
	
	public prmths_ApiClient() 
	{}

	public String getHost()
	{
		return prmths_ApiClient.host;
	}

	public String getHostUrl(String protocol, String path)
	{
		String urlTemplate = '{0}://{1}{2}';	
		String url = String.format(urlTemplate, new List<String>{protocol, this.getHost(), path});				

		return url;
	}


    public RequestBuilder request(String endpoint) 
    {
    	PageReference pf = new PageReference(endpoint);
    	RequestBuilder builder = new RequestBuilder(this, pf);
    	return builder;
    }

    public RequestBuilder request(String endpoint, Map<String, String> queryParams) 
    {
		PageReference pf = new PageReference(endpoint);
		pf.getParameters().putAll(queryParams);

    	RequestBuilder builder = new RequestBuilder(this, pf);
    	return builder;		
    }
}