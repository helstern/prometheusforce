public with sharing class prmths_OauthAccess 
{
    static public final TokensRepository tokenRepository;

    static {        
        tokenRepository = new TokensRepository(prometheus__Oauth2_Tokens__c.sObjectType);        
    }

    static public final ApiModelMapper apiModelMapper = new ApiModelMapper();

    public class ApiModelMapper
    {
        public ApiModelMapper()
        {}

        public prometheus__Oauth2_Tokens__c fromApiModel(prmths_ApiModels.Oauth2AccessGoogleCredential model) 
        {
            prometheus__Oauth2_Tokens__c tokens;
            tokens = new prometheus__Oauth2_Tokens__c(                
                prometheus__Access_Token__c = model.access_token,
                prometheus__Access_Token_Expires_In__c = model.expires_in,
                prometheus__Refresh_Token__c = model.refresh_token
            );            

            return tokens;
        }
    }

    public class IndividualAccessPreparedQuery implements prmths_Query.PreparedObjectQuery
    {
        private Schema.DescribeSObjectResult objectInfo; 

        /**
         * providerName binding
         */
        private String providerName;

        /**
         * userId binding
         */
        private String userId;

        public IndividualAccessPreparedQuery(Schema.DescribeSObjectResult objectInfo)
        {
            this.objectInfo = objectInfo;
        }

        public Schema.DescribeSObjectResult getObjectInfo() 
        {
            return this.objectInfo;
        }

        private void resolveBindings(Set<prmths_Query.QueryBindParameter> bindings)
        {
            for (prmths_Query.QueryBindParameter binding: bindings) {
                if (binding.getName() == 'providerName') {
                    providerName = binding.getValue();
                } else if (binding.getName() == 'userId') {
                    userId = binding.getValue();
                }
            }            
        }

        public List <prometheus__Oauth2_Tokens__c> toList(Set<prmths_Query.QueryBindParameter> bindings)
        {            
            this.resolveBindings(bindings);

            List <prometheus__Oauth2_Tokens__c> tokensList = 
            [
                SELECT 
                    Name,
                    prometheus__Access_Token__c,
                    prometheus__Access_Token_Expires_In__c,
                    prometheus__Refresh_Token__c,
                    prometheus__Oauth_Provider__c
                FROM 
                    prometheus__Oauth2_Tokens__c
                WHERE 
                    prometheus__Oauth_Provider__c = :providerName
                    AND Owner.Id = :userId
            ];
         
            return tokensList;            
        }        

        public prometheus__Oauth2_Tokens__c one(Set<prmths_Query.QueryBindParameter> bindings)
        {
            this.resolveBindings(bindings);

            List <prometheus__Oauth2_Tokens__c> tokensList =
            [
                SELECT 
                    Name,
                    prometheus__Access_Token__c,
                    prometheus__Access_Token_Expires_In__c,
                    prometheus__Refresh_Token__c,
                    prometheus__Oauth_Provider__c
                FROM 
                    prometheus__Oauth2_Tokens__c
                WHERE 
                    prometheus__Oauth_Provider__c = :providerName
                    AND Owner.Id = :userId
                LIMIT 2
            ];
         
            if (tokensList.size() == 1) {
                return tokensList.get(0);
            }

            return null;            
        }         
    }

    public class IndividualAccessSpecification
    {
        private String userId;

        private String providerName;

        public IndividualAccessSpecification(String userId, String providerName)
        {
            this.userId = userId;

            this.providerName = providerName;
        }

        public String getUserId()
        {
            return userId;
        }

        public String getProviderName()
        {
            return providerName;
        }

        public boolean isSatisfied(prometheus__Oauth2_Tokens__c tokens)
        {
            return tokens.Owner.Id == userId && tokens.prometheus__Oauth_Provider__c == providerName;
        }
    }

    public class TokensRepository 
    {
        private Schema.sObjectType objectType;

        private Schema.DescribeSObjectResult objectInfo;

        public TokensRepository(Schema.sObjectType objectType)
        {
            this.objectType = objectType;
            this.objectInfo = objectType.getDescribe();
        }

        public prmths_Domain.SpecificationExecutor find(IndividualAccessSpecification spec)
        {
            Set<prmths_Query.QueryBindParameter> bindParameters;
            prmths_Query.QueryBindParameter variable;

            bindParameters = new Set<prmths_Query.QueryBindParameter>();
            variable = new prmths_Query.QueryBindParameter('userId', spec.getUserId());
            bindParameters.add(variable);

            variable = new prmths_Query.QueryBindParameter('providerName', spec.getProviderName());
            bindParameters.add(variable);            

            prmths_Domain.SpecificationExecutor qe = new prmths_Query.PreparedQueryExecutor(
                new IndividualAccessPreparedQuery(objectInfo),
                bindParameters
            );

            return qe;
        }

        public void save(prometheus__Oauth2_Tokens__c tokens) 
        {            
            upsert tokens; // DMLException can be thrown here
        }
    }

    public class AccessService
    {
        private prmths_ApiClient apiClient;

        public AccessService(prmths_ApiClient apiClient)
        {
            this.apiClient = apiClient;
        }

        public prmths_ApiModels.Oauth2AccessGoogleCredential refreshAccess(String refreshToken)        
        {
            prmths_ApiModels.Oauth2AccessGoogleCredential credential;            
            credential = (prmths_ApiModels.Oauth2AccessGoogleCredential) 
            prmths_ApiResources.json.newOauth2RefreshAccessGoogle(
                apiClient, 
                prmths_ApiModels.Oauth2AccessGoogleCredential.class
            ).get(
                refreshToken
            );            

            return credential;
        }
    }

    public class StoredAccess
    {
        private prometheus__Oauth2_Tokens__c existingTokens;        

        private prometheus__Oauth2_Tokens__c newTokens;        

        public StoredAccess(prometheus__Oauth2_Tokens__c existingTokens)
        {
            this.existingTokens = existingTokens;
        }

        public String getToken()
        {        

            String accessToken = existingTokens.prometheus__Access_Token__c;        
            return accessToken;
        }

        public String renewAccess(AccessService accessService)
        {
            String refreshToken = existingTokens.prometheus__Refresh_Token__c;   
            prmths_ApiModels.Oauth2AccessGoogleCredential credential = accessService.refreshAccess(refreshToken);
            
            newTokens = prmths_OauthAccess.apiModelMapper.fromApiModel(credential);                        
            return credential.access_token;            
        }

        public Boolean persistTokens(TokensRepository repository)
        {
            if (null == newTokens) {
                System.debug('no tokens to persist');
                return false;
            }

            existingTokens.prometheus__Access_Token__c = newTokens.prometheus__Access_Token__c;
            existingTokens.prometheus__Access_Token_Expires_In__c = newTokens.prometheus__Access_Token_Expires_In__c;
            repository.save(existingTokens);            
            newTokens = null;

            System.debug('persisted new tokens');

            return true;
        }        
    }

    public prmths_OauthAccess() 
    {}    

    public static StoredAccess createCurrentUserAccess(String accessType, TokensRepository repository)
    {
        String currentUserId = UserInfo.getUserId();
        prmths_OauthAccess.IndividualAccessSpecification spec;                    
        spec = new prmths_OauthAccess.IndividualAccessSpecification(currentUserId, accessType);    
        prometheus__Oauth2_Tokens__c tokens;
        tokens = (prometheus__Oauth2_Tokens__c) prmths_OauthAccess.tokenRepository.find(spec).one();        
        if (null == tokens) {
            return null;
        }

        return new StoredAccess(tokens);
    }                
}