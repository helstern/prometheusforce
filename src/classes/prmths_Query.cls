public with sharing class prmths_Query 
{
    public class QueryBindParameter
    {
        private String name ;

        private String value;

        public QueryBindParameter(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        public String getName()
        {
            return name;            
        }

        public String getValue()
        {
            return value;
        }
    }

    public class PreparedQueryExecutor implements prmths_Domain.SpecificationExecutor
    {
        private Set<QueryBindParameter> bindings;

        private PreparedObjectQuery query;

        public PreparedQueryExecutor(PreparedObjectQuery query, Set<QueryBindParameter> bindings)
        {
            this.query = query;
            this.bindings = bindings.clone();            
        }

        public List<sObject> toList() 
        {
            return this.query.toList(bindings);
        }

        public sObject one()
        {
            return this.query.one(bindings);            
        }
    }

    public interface PreparedObjectQuery
    {
        Schema.DescribeSObjectResult getObjectInfo();

        List<sObject> toList(Set<QueryBindParameter> bindings);

        sObject one(Set<QueryBindParameter> bindings);
    }

}