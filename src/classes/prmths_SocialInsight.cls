public with sharing class prmths_SocialInsight 
{

    static public final SocialInsightRepository repository;

    static {        
        repository = new SocialInsightRepository(prometheus__prometheus__c.sObjectType);        
    }

    public class LeadPreparedQuery implements prmths_Query.PreparedObjectQuery
    {
        private Schema.DescribeSObjectResult objectInfo; 

        private Map<String, String> bindings;

        public LeadPreparedQuery(Schema.DescribeSObjectResult objectInfo)
        {
            this.objectInfo = objectInfo;
        }

        public Schema.DescribeSObjectResult getObjectInfo() 
        {
            return this.objectInfo;
        }

        private Map<String, String> bindingSetToMap(Set<prmths_Query.QueryBindParameter> bindings)
        {
            Map<String, String> mapBindings = new Map<String, String>();

            for (prmths_Query.QueryBindParameter binding: bindings) {
                mapBindings.put(binding.getName(), binding.getValue());
            }            
            return mapBindings;
        }

        public List <prometheus__prometheus__c> toList(Set<prmths_Query.QueryBindParameter> bindings)
        {            
            Map<String, String> bindingMap = this.bindingSetToMap(bindings);

            List <prometheus__prometheus__c> objectList = 
            [
                SELECT 
                    prometheus__googleplus_id__c,
                    Name
                FROM 
                    prometheus__prometheus__c
                WHERE 
                     Lead__r.Id  = :bindingMap.get('leadId')
            ];
         
            return objectList;            
        }        

        public prometheus__prometheus__c one(Set<prmths_Query.QueryBindParameter> bindings)
        {
            Map<String, String> bindingMap = this.bindingSetToMap(bindings);

            List <prometheus__prometheus__c> objectList = 
            [
                SELECT 
                    prometheus__googleplus_id__c,
                    Name
                FROM 
                    prometheus__prometheus__c
                WHERE 
                     Lead__r.Id = :bindingMap.get('leadId')
                LIMIT 2
            ];
         
            if (objectList.size() == 1) {
                return objectList.get(0);
            }

            return null;            
        }         
    }


    public class SocialInsightRepository
    {
        private Schema.sObjectType objectType;

        public SocialInsightRepository(Schema.sObjectType objectType)
        {
            this.objectType = objectType;
        }        

        public prmths_Domain.SpecificationExecutor findByLeadId(String id) {
            Set<prmths_Query.QueryBindParameter> bindParameters;
            bindParameters = new Set<prmths_Query.QueryBindParameter>();

            prmths_Query.QueryBindParameter variable;
            variable = new prmths_Query.QueryBindParameter('leadId', id);
            bindParameters.add(variable);

            prmths_Domain.SpecificationExecutor qe = new prmths_Query.PreparedQueryExecutor(
                new LeadPreparedQuery(
                    objectType.getDescribe()
                ),
                bindParameters
            );

            return qe;            
        }
    }
}