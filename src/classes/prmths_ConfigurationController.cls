public with sharing class prmths_ConfigurationController 
{
    class OauthAccess
    {
        private prometheus__Oauth2_Tokens__c tokens;

        private prometheus__Oauth2_Tokens__c newTokens;

        private prmths_ApiClient apiClient;

        OauthAccess(prometheus__Oauth2_Tokens__c tokens, prmths_ApiClient apiClient)
        {
            this.tokens = tokens;
            this.apiClient = apiClient;
        }

        public Boolean persistTokens(prmths_OauthAccess.TokensRepository repository)
        {
            if (null == newTokens) {
                System.debug('no tokens to persist');
                return false;
            }

            if (null == tokens) {
            	repository.save(newTokens);
				tokens = newTokens;
				newTokens = null;            		
				return false;
            }

            return false;
        }

        public String accessToken()
        {        
        	if (null == tokens) {
        		return null;
        	}

            String accessToken = tokens.prometheus__Access_Token__c;        
            return accessToken;
        }

        public String requestAccessToken(String authCode, String providerName)
        {
			prmths_ApiModels.Oauth2AccessGoogleCredential credential;
			credential = (prmths_ApiModels.Oauth2AccessGoogleCredential) 
				prmths_ApiResources.json.newOauth2AccessGoogle(
					apiClient, 
					prmths_ApiModels.Oauth2AccessGoogleCredential.class
				).get(
					authCode
				);        	

			newTokens = prmths_OauthAccess.apiModelMapper.fromApiModel(credential);
			newTokens.put('prometheus__Oauth_Provider__c', providerName);				

			return credential.access_token;
        }
    }

	
	private ApexPages.StandardController standardController;

	private OauthAccess oauthAccess;

	private prmths_ApiClient apiClient;

	private Boolean authorizationGranted;	
	
	private String authorizationCode;	

	public prmths_ConfigurationController(ApexPages.StandardController stdController) 
	{
		apiClient = new prmths_ApiClient();
		String accessToken = initializeOauthAccess(apiClient);
		
		if (null == accessToken) {
			this.authorizationGranted = false;									
		} else {
			this.authorizationGranted = true;
		}
	}

	private String initializeOauthAccess (prmths_ApiClient apiClient)
	{
		prmths_OauthAccess.IndividualAccessSpecification spec;
		spec = new prmths_OauthAccess.IndividualAccessSpecification(UserInfo.getUserId(), 'google');		

		prometheus__Oauth2_Tokens__c tokens;
		tokens = (prometheus__Oauth2_Tokens__c) prmths_OauthAccess.tokenRepository.find(spec).one();

		this.oauthAccess = new OauthAccess(tokens, apiClient);
		return this.oauthAccess.accessToken();
	}

	public String getAuthorizationCode() 
	{
		if (this.authorizationGranted == true) {
			return ''; 
		}

		return this.authorizationCode;		
	}

	public void setAuthorizationCode(String authorizationCode) 
	{
		this.authorizationCode = authorizationCode;	
	}  

	public PageReference authorize() 
	{		
		if (this.authorizationGranted == true) {
			System.debug('already authorized ' + this.authorizationCode + ' .');
			return null;
		}

		String code = this.authorizationCode;
		this.authorizationGranted = true;

		this.oauthAccess.requestAccessToken(code, 'google');
		this.oauthAccess.persistTokens(prmths_OauthAccess.tokenRepository);
		
		return null;
	}
}