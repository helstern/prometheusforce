public with sharing class prmths_ApiModelMappers 
{    
    public class SocialInsightFieldMapper
    {
        private String objectNamespacePrefix;

        //private Map<String, Schema.SObjectField> socialInsightFieldMetadata;

        public SocialInsightFieldMapper(String objectNamespacePrefix) 
        {
            this.objectNamespacePrefix = objectNamespacePrefix;
        }

        public prometheus__prometheus__c fromApiModel(prmths_ApiModels.SocialProfileMatch source) 
        {
            return fromApiModel(source.profile);
        }        

        public prometheus__prometheus__c fromApiModel(prmths_ApiModels.SocialProfileSummary source) 
        {                    
            prometheus__prometheus__c target = new prometheus__prometheus__c();
            target.put(prometheus__prometheus__c.prometheus__googleplus_id__c, source.id);            

            return target;
        }        
    }

    static public SocialInsightFieldMapper mapSocialInsightFields()
    {        
        //Map<String, Schema.SObjectField> fieldMeta = prometheus__c.sObjectType.getDescribe().fields.getMap();
        //http://salesforce.stackexchange.com/questions/28974/determining-namespace-prefix-in-javascript-and-apex/28977#28977
        String namespace = SObjectType.prometheus__c.Name.substringBefore('prometheus__c');
        return new SocialInsightFieldMapper(namespace);        
    }    

    public prmths_ApiModelMappers() 
    {}
}