public with sharing class prmths_SObjectUnitOfWork 
{	     
    interface Relationship
    {
        SObject resolve();
    }

    public class OneToManyRelationship implements Relationship
    {
        private SObject foreignKeyOwner;
        private Schema.SObjectField field;        
        private SObject other;

        public OneToManyRelationship(SObject foreignKeyOwner, Schema.SObjectField field, SObject other)        
        {
            this.foreignKeyOwner = foreignKeyOwner;
            this.field = field;
            this.other = other;
        }

        public SObject resolve()
        {
            foreignKeyOwner.put(field, other.Id);
            return foreignKeyOwner;
        }
    }

    private List<SObject> newSObjects;

    private List<Relationship> newRelationships;

    public prmths_SObjectUnitOfWork() 
    {
        this.newSObjects = new List<SObject>();     
        this.newRelationships = new List<Relationship>();		
	}

    public prmths_SObjectUnitOfWork registerNew(SObject newObject) 
    {
        this.newSObjects.add(newObject);
        return this;
    }

    public prmths_SObjectUnitOfWork registerNewRelationship(
        SObject forObject, 
        String fieldName, 
        SObject newRelated
    ) {
        Type clazz = resolveClass(forObject);
        // this getMap must be cached because of gvnr limits
        Map<String, Schema.SObjectField> fieldMetadata = forObject.getSObjectType().getDescribe().fields.getMap();
        Schema.SObjectField field = resolveField(clazz, fieldMetadata, fieldName);

        return registerNewRelationship(forObject, field, newRelated);
    }

    public prmths_SObjectUnitOfWork registerNewRelationship(
        SObject forObject, 
        Schema.SObjectField field, 
        SObject newRelated
    ) {        
        this.newSObjects.add(newRelated);
        //TO DO: determine the type of relationship
        Relationship rel = new OneToManyRelationship(forObject, field, newRelated);
        this.newRelationships.add(rel);

        return this;
    }

    private Type resolveClass(SObject forObject)
    {
        String clsName = String.valueOf(forObject).substringBefore(':');
        return Type.forName(clsName);
    }

    private Schema.SObjectField resolveField(
        Type clazz, 
        Map<String, Schema.SObjectField> fieldMetadata,
        String fieldName
    ) {        
        String namespacePrefix = resolveNamespacePrefix(clazz);
        List<String> candidateNames = getCandidateFieldNames(namespacePrefix, fieldName);

        for (String name : candidateNames) {
            if (fieldMetadata.containsKey(name)) {
                System.debug('name is ' + fieldMetadata.containsKey(name));
                return fieldMetadata.get(name);                    
            }
        }

        return null;
    }

    private String resolveNamespacePrefix(Type clazz)
    {
        String custom = clazz.getName().substringBefore('__c');
        if (String.isEmpty(custom)) {
            return '';
        }

        String namespacePrefix = custom.substringBefore('__') + '__';
        if (namespacePrefix.equals('__')) {
            return '';
        }

        return namespacePrefix;
    }

    private List<String> getCandidateFieldNames(String namespacePrefix, String fieldName)
    {        
        List <String> candidateNames = new List<String>{fieldName.toLowerCase(), fieldName.toLowerCase() + '__c'};
        if (! String.isEmpty(namespacePrefix)) {
            candidateNames.add(namespacePrefix.toLowerCase() + fieldName);
            candidateNames.add(namespacePrefix.toLowerCase() + fieldName.toLowerCase() + '__c');
        }
        return candidateNames;
    }

    public Boolean commitWork()
    {
        Savepoint sp = Database.setSavepoint();

        try {
            for (SObject newObject : newSObjects) {
                insert newObject;
            }

            List<SObject> newWithRelationships = new List<SObject>();
            SObject modified;
            for (Relationship rel : newRelationships) {
                modified = rel.resolve();
                //TO DO: create a set with ids of all the objects and add modified to list only if it is not in that set
                newWithRelationships.add(modified);
            }

            for (SObject newObject : newWithRelationships) {
                update newObject;
            }            
        } catch (Exception e) {
            Database.rollback(sp);
            throw e;
        }

        return true;
    }
}