public with sharing class prmths_ApiResources {

	static public final String prometheusHost = 'prometheus-1025.appspot.com';

	static public final JsonResource json = new JsonResource();

	public class JsonResource 
	{
		public DiscoverContact newDiscoverContact(prmths_ApiClient apiClient, Type model) 
		{
        	ffhttp_JsonDeserializer deserializer = new ffhttp_JsonDeserializer(model);
        	return new prmths_ApiResources.DiscoverContact(apiClient, deserializer);
		}

		public SearchContact newSearchContact(prmths_ApiClient apiClient, Type model) 
		{
        	ffhttp_JsonDeserializer deserializer = new ffhttp_JsonDeserializer(model);
        	return new prmths_ApiResources.SearchContact(apiClient, deserializer);
		}

    	public Oauth2AccessGoogle newOauth2AccessGoogle(prmths_ApiClient apiClient, Type model) 
    	{
        	ffhttp_JsonDeserializer deserializer = new ffhttp_JsonDeserializer(model);
    	    return new prmths_ApiResources.Oauth2AccessGoogle(apiClient, deserializer);
    	}      	

    	public Oauth2RefreshAccessGoogle newOauth2RefreshAccessGoogle(prmths_ApiClient apiClient, Type model) 
    	{
        	ffhttp_JsonDeserializer deserializer = new ffhttp_JsonDeserializer(model);
    	    return new prmths_ApiResources.Oauth2RefreshAccessGoogle(apiClient, deserializer);
    	}  
	}

	public class DiscoverContact
	{
		private prmths_ApiClient client;

		private ffhttp_IDeserialize responseDeserializer;

		private String path = '/discover/googleplus';

		public DiscoverContact (prmths_ApiClient client, ffhttp_IDeserialize responseDeserializer) 
		{			
			this.client = client;
			this.responseDeserializer = responseDeserializer;
		}

		public Object get(String similarWithProfileId, String accessToken) 
		{			
			String url = client.getHostUrl('http', this.path) + '/';

			Map<String, String> queryParams = new Map<String, String>();
			queryParams.put('profile_id', similarWithProfileId);
			queryParams.put('access_token', accessToken);			

			HttpResponse response = client.request(url, queryParams)
				.get()
				.accept('application/json')
				.send()
				.execute();
			String body = response.getBody();
			Object entity = responseDeserializer.deserialize(body);
			return entity;	
		}
	}

	public class SearchContact 
	{
		private prmths_ApiClient client;

		private ffhttp_IDeserialize responseDeserializer;

		private String path = '/search/googleplus';

		public SearchContact (prmths_ApiClient client, ffhttp_IDeserialize responseDeserializer) 
		{			
			this.client = client;
			this.responseDeserializer = responseDeserializer;
		}

		public Object get(String name, String accessToken) 
		{			
			String url = client.getHostUrl('http', this.path) + '/';

			Map<String, String> queryParams = new Map<String, String>();
			queryParams.put('q', name);
			queryParams.put('access_token', accessToken);			

			HttpResponse response = client.request(url, queryParams)
				.get()
				.accept('application/json')
				.send()
				.execute();
			String body = response.getBody();
			Object entity = responseDeserializer.deserialize(body);
			return entity;	
		}
	}

	public class Oauth2AccessGoogle 
	{	
		private prmths_ApiClient client;

		private ffhttp_IDeserialize responseDeserializer;

		private String path = '/access-oauth2/google/exchange-auth-code';
  
		public Oauth2AccessGoogle(prmths_ApiClient client, ffhttp_IDeserialize responseDeserializer) 
		{
			this.client = client;
			this.responseDeserializer = responseDeserializer;
		}

		public Object get(String authorizationCode) 
		{
			String urlTemplate = client.getHostUrl('http', this.path) + '/' + '?' + 'authorization_code={0}';			
			String url = String.format(urlTemplate, new List<String>{authorizationCode});

			HttpResponse response = client.request(url).get().accept('application/json').send().execute();
			String body = response.getBody();
			Object entity = responseDeserializer.deserialize(body);
			return entity;	
		}		
	}

	public class Oauth2RefreshAccessGoogle
	{
		private prmths_ApiClient client;

		private ffhttp_IDeserialize responseDeserializer;

		private String path = '/access-oauth2/google/exchange-refresh-token';

		public Oauth2RefreshAccessGoogle(prmths_ApiClient client, ffhttp_IDeserialize responseDeserializer) 
		{
			this.client = client;
			this.responseDeserializer = responseDeserializer;
		}

		public Object get(String refreshToken) 
		{
			String urlTemplate = client.getHostUrl('http', this.path) + '/' + '?' + 'refresh_token={0}';			
			String url = String.format(urlTemplate, new List<String>{refreshToken});

			HttpResponse response = client.request(url).get().accept('application/json').send().execute();
			String body = response.getBody();
			Object entity = responseDeserializer.deserialize(body);
			return entity;	
		}				
	}

	public prmths_ApiResources()	
	{}
}