/**
 * Created by radu on 30/09/15.
 */

public with sharing class prmths_SocInsightLeadBriefWdgtController {

    private ApexPages.StandardController standardController;

    public prmths_SocInsightLeadBriefWdgtController(ApexPages.StandardController controller) {
        standardController = controller;

        List<String> fields = getFields();
        standardController.addFields(fields);
        Lead record = (Lead)standardController.getRecord();        
    }

    public virtual ApexPages.StandardController getController() {
        return standardController;
    }

    public List<String> getFields() {
        List<String> fields = new List<String> { 'FirstName', 'LastName'};
        return fields;
    }
}