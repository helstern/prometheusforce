public with sharing class prmths_SocialInsightOverviewController 
{
    public class ViewProfileMatchChoice
    {
        private prmths_ApiModels.SocialProfileMatch profileMatch;

        public Boolean convertToLead {get; set;}

        public Boolean selected {get; set;}

        public ViewProfileMatchChoice(prmths_ApiModels.SocialProfileMatch profileMatch)
        {
            this.profileMatch = profileMatch;
            this.convertToLead = false;
            this.selected = false;
        }

        public prmths_ApiModels.SocialProfileMatch getProfileMatch()
        {
            return profileMatch;
        }

        public String getName() 
        {
            return profileMatch.profile.display_name;
        }

        public String getImage() 
        {
            return profileMatch.profile.image;
        }
    }

    public class ViewModel
    {
        public List<ViewProfileMatchChoice> profileChoices {get; set;}    

        public Boolean displayProfileChoices {get; set;}    

        public Boolean chooseSimilarProfiles {get; set;}    

        public Boolean chooseProfile {get; set;}    

        public Integer columns {get; set;}    

        public ViewModel()
        {            
            chooseSimilarProfiles = false;
            chooseProfile = false;
            displayProfileChoices  = false;
            profileChoices = new List<ViewProfileMatchChoice>();
            columns = 5;
        }

        public String getProfilesString()
        {
            return JSON.serializePretty(profileChoices);            
        }
    }

    class Context
    {
        private Lead lead;

        private prometheus__prometheus__c prometheusLead;

        public Lead getLead()
        {
            return lead;
        }

        public Lead setLead(Lead lead)
        {
            Lead oldLead = this.lead;
            this.lead = lead;

            return oldLead;
        }

        public prometheus__prometheus__c getPrometheusLead()
        {
            return prometheusLead;
        }

        public prometheus__prometheus__c setPrometheusLead(prometheus__prometheus__c lead)
        {
            prometheus__prometheus__c oldLead = this.prometheusLead;
            this.prometheusLead = lead;

            return oldLead;            
        } 
    }

    interface State
    {}    

    interface ControllerActions extends State
    {       
        void configureViewModel(ViewModel viewModel);        
    }

    interface ControllerState extends ControllerActions
    {
        StateActionsTrigger getActionsTrigger(StateActionsTriggersContainer container);
    }

    interface StateActionsTrigger
    {
        OutgoingStateTransitions getOutgoingTransitions();

        ControllerState onInit(ControllerState currentState, Context ctx, Lead lead);

        ControllerState onFindSimilarProfiles(ControllerState currentState, Context ctx);

        ControllerState onFindProfile(ControllerState currentState, Context ctx);
    }

    abstract class AbstractInactiveStateActionsTrigger
    {
        public virtual ControllerState onInit(ControllerState currentState, Context ctx, Lead lead)
        {
            return null;
        }

        public virtual ControllerState onFindSimilarProfiles(ControllerState currentState, Context ctx)
        {
            return null;
        }

        public virtual ControllerState onFindProfile(ControllerState currentState, Context ctx) 
        {
            return null;
        }
    }    

    interface StateActionsTriggersContainer 
    {
        StateActionsTrigger getActionsTrigger(ControllerState state);

        StateActionsTrigger getActionsTrigger(StateInit state);

        StateActionsTrigger getActionsTrigger(StateChooseSimilarProfiles state);

        StateActionsTrigger getActionsTrigger(StateChooseProfile state);
    }

    class ControllerStateActionsTriggersContainer implements StateActionsTriggersContainer 
    {
        private OutgoingStateTransitions transitions;
        
        public ControllerStateActionsTriggersContainer(OutgoingStateTransitions transitions)
        {
            this.transitions = transitions;            
        }

        public StateActionsTrigger getActionsTrigger(ControllerState state)
        {
            System.debug('ControllerStateActionsTriggersContainer.getActionsTrigger ControllerState');
            return null;
        }

        public StateActionsTrigger getActionsTrigger(StateInit state)
        {
            System.debug('ControllerStateActionsTriggersContainer.getActionsTrigger StateInit');
            return new StateInitActionsTrigger(state, transitions);
        }

        public StateActionsTrigger getActionsTrigger(StateChooseSimilarProfiles state)
        {
            return new FindSimilarProfilesListTrigger(state, transitions);
        }

        public StateActionsTrigger getActionsTrigger(StateChooseProfile state)
        {
            return new FindProfilesListTrigger(state, transitions);
        }
    }

    interface ControllerStateTransition
    {
        ControllerState getSource();

        ControllerState getTarget();    

        Boolean evaluateContext(Context ctx);
    }

    class ControllerStateTransitionException extends Exception
    {}

    class ControllerStandardStateTransition implements ControllerStateTransition
    {
        private ControllerState source;

        private ControllerState target;

        private ContextGuard guard;

        public ControllerStandardStateTransition(
            ControllerState source, 
            ControllerState target,
            ContextGuard guard
        ){
            this.source = source;
            this.target = target;
            this.guard = guard;
        }

        public ControllerState getSource()
        {
            return source;
        }

        public ControllerState getTarget()
        {
            return target;
        }

        public Boolean evaluateContext(Context ctx)
        {
            return guard.evaluate(ctx);
        }
    }

    interface OutgoingStateTransitions
    {        
        Boolean postInitAction(StateInit source, StateTransitionEvaluator accumulate);
    }        

    interface ContextGuard
    {
        Boolean evaluate(Context ctx);
    }

    class PrometheusLeadExists implements ContextGuard
    {
        public Boolean evaluate(Context ctx)
        {
            return (ctx.getLead() != null) && (ctx.getPrometheusLead() != null);
        }
    }

    class PrometheusLeadDoesNotExist implements ContextGuard
    {
        public Boolean evaluate(Context ctx)
        {
            return (ctx.getLead() != null) && (ctx.getPrometheusLead() == null);
        }
    }

    class StateTransitionEvaluator 
    {
        private List<ControllerStateTransition> accumulate;

        public StateTransitionEvaluator()
        {
            this.accumulate = new List<ControllerStateTransition>();
        }

        public Boolean add(ControllerStateTransition transition) 
        {
            accumulate.add(transition);
            return true;
        }

        public Boolean isEmpty()
        {
            return accumulate.isEmpty();
        }

        public ControllerStateTransition evaluate(Context ctx)
        {
            System.debug('evaluate transitions');
            if (isEmpty()) {
                throw new ControllerStateTransitionException('no transitions to evaluate ');
            }

            ControllerStateTransition transition;            
            for (ControllerStateTransition stateTransition : accumulate) {
                if (stateTransition.evaluateContext(ctx)) {
                    if (null == transition) {
                        transition = stateTransition;
                    } else {
                        throw new ControllerStateTransitionException('too many transitions can fire');
                    }
                }
            }

            if (null == transition) {
                throw new ControllerStateTransitionException('no transition can fire');
            }            

            return transition;
        }
    }

    class ControllerOutgoingStateTransitions implements OutgoingStateTransitions
    {
        private StateInit stateInit;

        private StateChooseSimilarProfiles stateFindSimilar;

        private StateChooseProfile stateFind;

        public ControllerOutgoingStateTransitions(
            StateInit stateInit, 
            StateChooseSimilarProfiles stateFindSimilar, 
            StateChooseProfile stateFind
        ) {
            this.stateInit = stateInit;
            this.stateFindSimilar = stateFindSimilar;
            this.stateFind = stateFind;
        }

        public Boolean postInitAction(
            StateInit source, 
            StateTransitionEvaluator accumulate
        ) {
            accumulate.add(
                new ControllerStandardStateTransition(
                    source, 
                    stateFindSimilar,
                    new PrometheusLeadExists()
                )
            );
            accumulate.add(
                new ControllerStandardStateTransition(
                    source, 
                    stateFind,
                    new PrometheusLeadDoesNotExist()
                )
            );

            return true;
        }
    }

    class StateInitActionsTrigger extends AbstractInactiveStateActionsTrigger implements StateActionsTrigger
    {
        private StateInit state;    

        private OutgoingStateTransitions transitions;

        public StateInitActionsTrigger(StateInit state, OutgoingStateTransitions transitions)
        {
            this.state = state;
            this.transitions = transitions;
        }

        public OutgoingStateTransitions getOutgoingTransitions()
        {
            return transitions;
        }

        public override ControllerState onInit(ControllerState currentState, Context ctx, Lead lead)
        {
            StateTransitionEvaluator transitions = new StateTransitionEvaluator();            
            state.executeInitAction(this, transitions, ctx, lead);

            if (transitions.isEmpty()) {
                return currentState;
            }

            return transitions.evaluate(ctx).getTarget();
        }
    }

    class StateInit implements ControllerState
    {
        private prmths_SocialInsight.SocialInsightRepository prometheusRepo;        

        public StateInit(prmths_SocialInsight.SocialInsightRepository prometheusRepo)
        {
            this.prometheusRepo = prometheusRepo;   
        }

        public StateActionsTrigger getActionsTrigger(StateActionsTriggersContainer container)
        {
            System.debug('StateInit.getActionsTrigger');
            return container.getActionsTrigger(this);
        }

        public Boolean executeInitAction(
            StateActionsTrigger triggeredBy,
            StateTransitionEvaluator accumulate,
            Context ctx,             
            Lead lead            
        ) {
            ctx.setLead(lead);
            String leadId = lead.Id;
            prometheus__prometheus__c prometheusLead = (prometheus__prometheus__c) prometheusRepo.findByLeadId(leadId).one();                        
            ctx.setPrometheusLead(prometheusLead);

            triggeredBy.getOutgoingTransitions().postInitAction(this, accumulate);    
            return true;
        }

        public void configureViewModel(ViewModel viewModel)
        {
            viewModel.chooseSimilarProfiles = false;            
            viewModel.chooseProfile = false;            
            viewModel.displayProfileChoices = false;
            viewModel.profileChoices = null;            
        }                
    }

    class FindSimilarProfilesListTrigger extends AbstractInactiveStateActionsTrigger implements StateActionsTrigger
    {
        private StateChooseSimilarProfiles state;

        private OutgoingStateTransitions transitions;        
        
        public FindSimilarProfilesListTrigger(StateChooseSimilarProfiles state, OutgoingStateTransitions transitions)
        {
            this.state = state;
            this.transitions = transitions;
        }

        public OutgoingStateTransitions getOutgoingTransitions()
        {
            return transitions;
        }

        public override ControllerState onFindSimilarProfiles(ControllerState currentState, Context ctx)
        {
            StateTransitionEvaluator transitions = new StateTransitionEvaluator();

            prometheus__prometheus__c prometheusLead = ctx.getPrometheusLead();
            String profileId = prometheusLead.prometheus__googleplus_id__c;

            state.executeLoadSimilarProfilesListAction(this, transitions, ctx, profileId);            

            if (transitions.isEmpty()) {
                return currentState;
            }

            return transitions.evaluate(ctx).getTarget();
        }
    }

    class StateChooseSimilarProfiles implements ControllerState
    {
        private SocialProfileMatchService service;

        private prmths_OauthAccess.AccessService accessService;

        private prmths_OauthAccess.StoredAccess access;

        private List<prmths_ApiModels.SocialProfileMatch> profiles;

        public StateChooseSimilarProfiles(
            SocialProfileMatchService service, 
            prmths_OauthAccess.AccessService accessService,
            prmths_OauthAccess.StoredAccess access
        ) {
            this.service = service;
            this.accessService = accessService;
            this.access = access;            

            this.profiles = new List<prmths_ApiModels.SocialProfileMatch>();
        }

        public StateActionsTrigger getActionsTrigger(StateActionsTriggersContainer container)
        {
            return container.getActionsTrigger(this);
        }

        public Boolean executeLoadSimilarProfilesListAction(
            StateActionsTrigger triggeredBy,
            StateTransitionEvaluator accumulate,
            Context ctx,             
            String profileId
        ) {
            String accessToken = access.getToken();                           
            Boolean refreshAccess = false;                    

            try {
                profiles = service.discoverProfiles(profileId, accessToken);
            } catch (prmths_ApiClient.ApiException e) {
                refreshAccess = true;
            }

            if (profiles != null && refreshAccess == false) {                        
                return true;            
            }        

            accessToken = access.renewAccess(accessService);
            profiles = service.discoverProfiles(profileId, accessToken);
            
            access.persistTokens(prmths_OauthAccess.tokenRepository);
            return true;                
        }                        

        public void configureViewModel(ViewModel viewModel)
        {        
            System.debug('StateChooseSimilarProfiles.configureViewModel');

            viewModel.chooseSimilarProfiles = true;            
            viewModel.chooseProfile = ! viewModel.chooseSimilarProfiles;            
            viewModel.displayProfileChoices = ! profiles.isEmpty();

            List <ViewProfileMatchChoice> profileChoices = new List<ViewProfileMatchChoice>();
            for (prmths_ApiModels.SocialProfileMatch profileMatch : profiles) {
                profileChoices.add(new ViewProfileMatchChoice(profileMatch));
            }
            viewModel.profileChoices = profileChoices;   
        }
    }    

    class FindProfilesListTrigger extends AbstractInactiveStateActionsTrigger implements StateActionsTrigger
    {
        private StateChooseProfile state;

        private OutgoingStateTransitions transitions;        
        
        public FindProfilesListTrigger(StateChooseProfile state, OutgoingStateTransitions transitions)
        {
            this.state = state;
            this.transitions = transitions;
        }

        public OutgoingStateTransitions getOutgoingTransitions()
        {
            return transitions;
        }

        public override ControllerState onFindProfile(ControllerState currentState, Context ctx)
        {
            StateTransitionEvaluator transitions = new StateTransitionEvaluator();

            Lead lead = ctx.getLead();
            state.executeLoadProfilesListAction(this, transitions, ctx, lead);

            if (transitions.isEmpty()) {
                return currentState;
            }

            return transitions.evaluate(ctx).getTarget();
        }
    }
 
    class StateChooseProfile implements ControllerState
    {
        private SocialProfileMatchService service;

        private prmths_OauthAccess.AccessService accessService;

        private prmths_OauthAccess.StoredAccess access;

        private List<prmths_ApiModels.SocialProfileMatch> profiles;

        public StateChooseProfile(
            SocialProfileMatchService service, 
            prmths_OauthAccess.AccessService accessService,
            prmths_OauthAccess.StoredAccess access
        ) {
            this.service = service;
            this.accessService = accessService;            
            this.access = access;            

            this.profiles = new List<prmths_ApiModels.SocialProfileMatch>();            
        }

        public StateActionsTrigger getActionsTrigger(StateActionsTriggersContainer container)
        {
            return container.getActionsTrigger(this);
        }

        public Boolean executeLoadProfilesListAction(
            StateActionsTrigger triggeredBy,
            StateTransitionEvaluator accumulate,
            Context ctx,                                     
            Lead lead
        ) {
            String query = lead.FirstName + '+' + lead.LastName;
            String accessToken = access.getToken();                

            Boolean refreshAccess = false;                    
            try {
                profiles = service.searchProfiles(query, accessToken);
            } catch (prmths_ApiClient.ApiException e) {
                refreshAccess = true;
            }

            if (profiles != null && refreshAccess == false) {                        
                return true;            
            }
        
            accessToken = access.renewAccess(accessService);  
            profiles = service.searchProfiles(query, accessToken);        

            access.persistTokens(prmths_OauthAccess.tokenRepository);
            return true;
        }                

        public void configureViewModel(ViewModel viewModel)
        {
            viewModel.chooseProfile = true;            
            viewModel.chooseSimilarProfiles = ! viewModel.chooseProfile;            


            viewModel.displayProfileChoices = ! profiles.isEmpty();

            List <ViewProfileMatchChoice> profileChoices = new List<ViewProfileMatchChoice>();
            for (prmths_ApiModels.SocialProfileMatch profileMatch : profiles) {
                profileChoices.add(new ViewProfileMatchChoice(profileMatch));
            }
            viewModel.profileChoices = profileChoices;   
        }
    }

    class SocialProfileMatchService
    {
        private prmths_ApiClient apiClient;

        public SocialProfileMatchService(prmths_ApiClient apiClient)        
        {
            this.apiClient = apiClient;
        }

        public List<prmths_ApiModels.SocialProfileMatch> searchProfiles(            
            String query, 
            String accessToken
        ) {
            Type representation = List<prmths_ApiModels.SocialProfileMatch>.class;
            prmths_ApiResources.SearchContact resource;
            resource = prmths_ApiResources.json.newSearchContact(apiClient, representation);

            List<prmths_ApiModels.SocialProfileMatch> profiles;
            profiles = (List<prmths_ApiModels.SocialProfileMatch>) resource.get(query, accessToken);
            return profiles;
        }

        public List<prmths_ApiModels.SocialProfileMatch> discoverProfiles(
            String similarWithProfiId, 
            String accessToken
        ) {
            Type representation = List<prmths_ApiModels.SocialProfileMatch>.class;
            prmths_ApiResources.DiscoverContact resource;
            resource = prmths_ApiResources.json.newDiscoverContact(apiClient, representation);

            List<prmths_ApiModels.SocialProfileMatch> profiles;
            profiles = (List<prmths_ApiModels.SocialProfileMatch>) resource.get(similarWithProfiId, accessToken);
            return profiles;
        }                        
    }
    
    public ViewModel viewModel {get; set;}
        
    private Context ctx;

    private ControllerState state;

    private ControllerOutgoingStateTransitions transitions;

    public prmths_SocialInsightOverviewController() 
    {
        System.debug('constructor called');
        viewModel = new ViewModel();     
    
        ctx = new Context();

        prmths_OauthAccess.StoredAccess access;
        access = prmths_OauthAccess.createCurrentUserAccess('google', prmths_OauthAccess.tokenRepository);

        prmths_ApiClient apiClient = new prmths_ApiClient();
        
        state = initializeStates(apiClient, access);
        state.configureViewModel(viewModel);
    }

    private ControllerState initializeStates(prmths_ApiClient apiClient, prmths_OauthAccess.StoredAccess access)
    {    
        SocialProfileMatchService matchService = new SocialProfileMatchService(apiClient);
        prmths_OauthAccess.AccessService accessService = new prmths_OauthAccess.AccessService(apiClient);

        StateInit stateInit = new StateInit(prmths_SocialInsight.repository);
        StateChooseSimilarProfiles stateFindSimilar = new StateChooseSimilarProfiles(matchService, accessService, access);
        StateChooseProfile stateFind = new StateChooseProfile(matchService, accessService, access);

        transitions = new ControllerOutgoingStateTransitions(
            stateInit,
            stateFindSimilar,
            stateFind
        );

        return stateInit;
    }

    private StateActionsTrigger getStateActionsTrigger()
    {
        ControllerStateActionsTriggersContainer triggers;
        triggers = new ControllerStateActionsTriggersContainer(transitions);

        StateActionsTrigger actionsTrigger = state.getActionsTrigger(triggers);
        return actionsTrigger;        
    }

    public void setInitialLead(Lead lead)    
    {
        System.debug('prmths_SocialInsightOverviewController.setInitialLead');

        StateActionsTrigger actionsTrigger = getStateActionsTrigger();    
        ControllerState newState = actionsTrigger.onInit(state, ctx, lead);
        if (null != newState) {
            System.debug('set new state');         
            this.state = newState;
            this.state.configureViewModel(viewModel);
        }    
    }

    public Lead getInitialLead()
    {
        return ctx.getLead();
    }

    public PageReference findSimilarSocialProfiles()
    {
        System.debug('prmths_SocialInsightOverviewController.findSimilarSocialProfiles');        
        StateActionsTrigger actionsTrigger = getStateActionsTrigger();
        ControllerState newState = actionsTrigger.onFindSimilarProfiles(state, ctx);
        if (null != newState) {
            System.debug('set new state');         
            this.state = newState;            
            this.state.configureViewModel(viewModel);
        }
        
        return null;
    }

    public PageReference findSocialProfile() 
    {
        System.debug('prmths_SocialInsightOverviewController.findSocialProfile');         

        StateActionsTrigger actionsTrigger = getStateActionsTrigger();
        ControllerState newState = actionsTrigger.onFindProfile(state, ctx);
        if (null != newState) {
            System.debug('set new state');         
            this.state = newState;            
            this.state.configureViewModel(viewModel);
        }
        
        return null;
    }

    public PageReference processSimilarProfiles()
    {
        System.debug('convert to lead');

        List<prmths_ApiModels.SocialProfileMatch> selectedList = new List<prmths_ApiModels.SocialProfileMatch>();
        for (ViewProfileMatchChoice choice : viewModel.profileChoices) {
            if (choice.convertToLead) {
                selectedList.add(choice.getProfileMatch());                
            }
        }

        if (selectedList.size() == 0) {
            return null;
        }

        if (selectedList.size() > 1) {
            return null;
        }

    
        prmths_SObjectUnitOfWork uow = new prmths_SObjectUnitOfWork();

        prmths_ApiModels.SocialProfileMatch selected = selectedList.get(0);        
        SObject prometheusSObject = prmths_ApiModelMappers.mapSocialInsightFields().fromApiModel(selected);
        uow.registerNew(prometheusSObject);

        Lead targetLead = new Lead();                
        prmths_SObjectMapper.SimpleCopyMapper mapper = prmths_SObjectMapper.createLeadCopyMapper();
        mapper.mapObject(targetLead, ctx.getLead(), selected);                
        uow.registerNewRelationship(prometheusSObject, 'Lead', targetLead);
        uow.commitWork();    

        return null;
    

        //Lead aLead = new Lead();
        //aLead.FirstName = selected.profile.display_name;        
        //   // Get the Meta Data for Foo__c  
        //Schema.DescribeSObjectResult recordMeta = aLead.getSObjectType().getDescribe();          
        ////String url = '/' + recordMeta.getKeyPrefix() + '/e?' + queryString
        //String url = '/' + recordMeta.getKeyPrefix() + '/e';
        //// Create PageReference for creating a new sObject and add any inbound query string parameters.  
    
        //PageReference aLeadPage = new PageReference(url);

        //aLeadPage.setRedirect(true);
        //return aLeadPage;
    }
}